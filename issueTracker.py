#! /usr/bin/env python

import datetime
import settings as s


class SubmitDateOutsideWorkingHoursException(Exception):
    pass


def timeStringToDatetime(anyDate, timeString):
    """
    Converts string in format "%I:%M %p" to datetime.datetime. Year, month and day will be taken from anyDate.
    :param anyDate: datetime.datetime
    Used to extract year, month and day.
    :param timeString: string
    contains time in format "%I:%M %p"
    :return: datetime.datetime
    """

    timeDatetime = datetime.datetime.strptime(timeString, "%I:%M %p")
    return datetime.datetime(
        anyDate.year, anyDate.month, anyDate.day, timeDatetime.hour, timeDatetime.minute)


def isInWorkingHours(submitDate):
    """
    It checks, whether given date is within working hours.
    :param submitDate: datetime.datetime
    :return: True, or False
    """

    if submitDate.weekday() in [0, 1, 2, 3, 4] \
            and timeStringToDatetime(submitDate, s.officeTime['OPEN']) <= submitDate \
                    <= timeStringToDatetime(submitDate, s.officeTime['CLOSE']):
        return True
    else:
        return False


def getNextWorkingDay(anyDate):
    """
    This function returns the same time on the next working day.
    :param anyDate: datetime.datetime
    An arbritrary date.
    :return: datetime.datetime
    The next working day at the same time.
    """

    if anyDate.weekday() == 4:
        anyDate += datetime.timedelta(days=3)
    elif anyDate.weekday() == 5:
        anyDate += datetime.timedelta(days=2)
    else:
        anyDate += datetime.timedelta(days=1)
    return anyDate


def getTimeLeftFromTheDay(anyDate, closeTime):
    """
    This function returns the time left from the actual day in hours.
    :param anyDate: datetime.datetime
    An arbritrary date.
    :param closeTime: string
    When the workday is ending. Format: "%I:%M %p". For instance "5:30 AM"
    :param openTime: string
    When the workday is starting. Format: "%I:%M %p". For instance "5:00 PM"
    :return: float
    Time left from the actual day in hours.
    """

    if not isInWorkingHours(anyDate):
        raise SubmitDateOutsideWorkingHoursException(
            "The date {} is outside of working hours".format(str(anyDate)))

    closeTimeInDatetime = timeStringToDatetime(anyDate, closeTime)
    return (closeTimeInDatetime - anyDate).seconds/3600.0


def CalculateDueDate(submitDate, turnaroundTime):
    """
    It calculates the due date recursively the following way:
    if dueDate will be today, which means timeLeft >= turnaroundtime, submitDate + turnaroundtime will be returned.
    Otherwise we move to the next working day. Due to this move the dueDate will be set to the next openTime
    (when the next working day starts), the time left from the current day will be substracted from turnaround time,
    and calculator will be invoked again on the new dueDate. Finally at the last iteration the timeLeft >= turnaroundTime condition
    will be true.
    :param submitDate: datetime.datetime
    Submit date of the issue.
    :param turnaroundTime: int
    Turnaround time in working hours.
    :return: datetime.datetime
    Due date.
    """

    if not isInWorkingHours(submitDate):
        raise SubmitDateOutsideWorkingHoursException(
            "The submit date {} is outside of working hours".format(str(submitDate)))

    timeLeft = getTimeLeftFromTheDay(submitDate, s.officeTime['CLOSE'])
    if timeLeft >= turnaroundTime:
        return submitDate + datetime.timedelta(hours=turnaroundTime)
    else:
        submitDate = timeStringToDatetime(getNextWorkingDay(submitDate), s.officeTime['OPEN'])
        return CalculateDueDate(submitDate, turnaroundTime-timeLeft)
