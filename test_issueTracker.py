#! /usr/bin/env python

import unittest

from issueTracker import getNextWorkingDay, getTimeLeftFromTheDay, timeStringToDatetime
from issueTracker import CalculateDueDate, isInWorkingHours, SubmitDateOutsideWorkingHoursException
import datetime
import settings as s


class IssueTrackerTestCase(unittest.TestCase):
    """Testing issueTracker functions."""

    def test_isInWorkingHoursFalse(self):
        """Testing if given date is in working hours."""

        submitDate = datetime.datetime(2017, 9, 18, 18, 0)

        self.assertEqual(
            isInWorkingHours(submitDate), False, "Given date is outside of working hours."
        )

    def test_isInWorkingHoursTrue(self):
        """Testing if given date is in working hours."""

        submitDate = datetime.datetime(2017, 9, 18, 14, 0)

        self.assertEqual(
            isInWorkingHours(submitDate), True, "Given date is inside working hours."
        )

    def test_timeStringToDatetime(self):
        """Testing if conversion is ok."""

        anyDate = datetime.datetime(2017, 9, 18, 21, 19)
        timeString = s.officeTime['CLOSE']

        shouldBeDate = datetime.datetime(2017, 9, 18, 17, 30)

        self.assertEqual(
            timeStringToDatetime(anyDate, timeString),  shouldBeDate, "Conversion failed."
        )

    def test_getNextWorkingDayFriday(self):
        """Testing for the next working day of Friday."""

        currentDate = datetime.datetime(2017, 9, 22, 12, 19)
        shouldBeDate = datetime.datetime(2017, 9, 25, 12, 19)

        self.assertEqual(
            getNextWorkingDay(currentDate), shouldBeDate, "Testing for a Friday failed"
        )

    def test_getNextWorkingDayMonday(self):
        """Testing for the next working day of Monday."""

        currentDate = datetime.datetime(2017, 9, 18, 12, 19)
        shouldBeDate = datetime.datetime(2017, 9, 19, 12, 19)

        self.assertEqual(
            getNextWorkingDay(currentDate), shouldBeDate, "Testing for a Monday failed"
        )

    def test_getNextWorkingDaySaturday(self):
        """Testing for the next working day of Saturday."""

        currentDate = datetime.datetime(2017, 9, 16, 12, 19)
        shouldBeDate = datetime.datetime(2017, 9, 18, 12, 19)

        self.assertEqual(
            getNextWorkingDay(currentDate), shouldBeDate, "Testing for a Saturday failed."
        )

    def test_getTimeLeftFromTheDay(self):
        """Testing if time left is calculated fine."""

        currentDate = datetime.datetime(2017, 9, 28, 11, 0)
        closeTime = s.officeTime['CLOSE']
        self.assertEqual(
            getTimeLeftFromTheDay(currentDate, closeTime), 6.5,
                "Testing for time passed and left for 11:00 until 05:30 PM failed."
        )

    def test_getTimeLeftFromTheDayNotInWorkingHours(self):
        """Testing if exception is raised for date outside working hours."""

        with self.assertRaises(SubmitDateOutsideWorkingHoursException):
            currentDate = datetime.datetime(2017, 9, 19, 6, 0)
            closeTime = s.officeTime['CLOSE']
            getTimeLeftFromTheDay(currentDate, closeTime)

    def test_CalculateDueDateDueToday(self):
        """Testing if due date is calculated fine."""

        submitDate = datetime.datetime(2017, 9, 28, 11, 0)
        turnaroundTime = 3
        shouldBeDate = datetime.datetime(2017, 9, 28, 14, 0)

        self.assertEqual(
            CalculateDueDate(submitDate, turnaroundTime), shouldBeDate, "Testing for due date due today failed."
        )

    def test_CalculateDueDateDueTomorrow(self):
        """Testing if due date is calculated fine."""

        submitDate = datetime.datetime(2017, 9, 28, 11, 0)
        turnaroundTime = 7
        shouldBeDate = datetime.datetime(2017, 9, 29, 9, 30)

        self.assertEqual(
            CalculateDueDate(submitDate, turnaroundTime), shouldBeDate, "Testing for due date due tomorrow failed."
        )

    def test_CalculateDueDateDueInTwoDays(self):
        """Testing if due date is calculated fine."""

        submitDate = datetime.datetime(2017, 9, 26, 11, 0)
        turnaroundTime = 16
        shouldBeDate = datetime.datetime(2017, 9, 28, 10, 0)

        self.assertEqual(
            CalculateDueDate(submitDate, turnaroundTime), shouldBeDate, "Testing for due date due in 2 days failed."
        )

    def test_CalculateDueDateDueInTwoDaysFromFriday(self):
        """Testing if due date is calculated fine."""

        submitDate = datetime.datetime(2017, 9, 22, 11, 1)
        turnaroundTime = 16
        shouldBeDate = datetime.datetime(2017, 9, 26, 10, 1)

        self.assertEqual(
            CalculateDueDate(submitDate, turnaroundTime), shouldBeDate, "Testing for 2 days from a Friday failed."
        )

    def test_CalculateDueDateNotInWokringHoursWeekend(self):
        """Testing if exception is raised for submit date on weekend."""

        with self.assertRaises(SubmitDateOutsideWorkingHoursException):
            submitDate = datetime.datetime(2017, 9, 23, 12, 0)
            turnaroundTime = 1
            CalculateDueDate(submitDate, turnaroundTime)

    def test_CalculateDueDateNotInWokringHoursWeekday(self):
        """Testing if exception is raised for submit date on weekdays outside working hours."""

        with self.assertRaises(SubmitDateOutsideWorkingHoursException):
            submitDate = datetime.datetime(2017, 9, 19, 6, 0)
            turnaroundTime = 1
            CalculateDueDate(submitDate, turnaroundTime)


if __name__ == '__main__':
    unittest.main()
